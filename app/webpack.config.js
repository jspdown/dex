const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const BUILD_DIR = path.resolve(__dirname, 'dist');
const APP_DIR = path.resolve(__dirname, 'src');
const STYLE_DIR = path.resolve(__dirname, 'styles');

const config = {
  devtool: 'eval-source-map',
  entry: [
    `${APP_DIR}/index.jsx`,
    `${STYLE_DIR}/index.scss`
  ],
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
    sourceMapFilename: '[name].js.map'
  },
  module: {
    loaders: [{
      test: /\.jsx?/,
      include: APP_DIR,
      loader: 'babel-loader'
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract([
        'css-loader',
        'sass-loader'
      ])
    }, {
      test: /\.png$/,
      loader: 'file-loader'
    }]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'bundle.css',
      allChunks: true
    })
  ]
};

module.exports = config;
