'use strict';

import update from 'immutability-helper';
import {
  WEB3_CONNECTED,
  WEB3_DISCONNECTED,
  WEB3_METAMASK_MISSING,
  WEB3_CONNECTION_ERROR,
  WEB3_ADDRESS_CHANGED
} from '../constants/actionTypes';

const defaultState = {
  metaMaskInstalled: true,
  network: null,
  address: null,
  isConnected: false
};

function account(state = defaultState, action) {
  switch (action.type) {
    case WEB3_METAMASK_MISSING:
      return update(state, { $merge: {
        metaMaskInstalled: false
      } });
    case WEB3_CONNECTED:
      return update(state, { $merge: {
        metaMaskInstalled: true,
        isConnected: true,
        address: action.data.address,
        network: action.data.network
      } });
    case WEB3_ADDRESS_CHANGED:
      return update(state, { $merge: {
        address: action.data.address
      } });
    case WEB3_DISCONNECTED:
      return defaultState;
    case WEB3_CONNECTION_ERROR:
    default:
      return state;
  }
}

export default account;
