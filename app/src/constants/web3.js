'use strict';

export const Networks = {
  MAIN_NET: '1',
  ROSTEN_TEST_NEXT: '3',
  RINKEBY_TEST_NEXT: '4',
  KOVAN_TEST_NET: '42'
};
