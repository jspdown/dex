'use strict';

const Promise = require('bluebird');

import {
  WEB3_METAMASK_MISSING,
  WEB3_CONNECTED,
  WEB3_DISCONNECTED,
  WEB3_CONNECTION_ERROR,
  WEB3_ADDRESS_CHANGED,
  WEB3_NETWORK_CHANGED
} from '../constants/actionTypes';

class Web3Wrapper {
  constructor(store, options) {
    this.options = Object.assign({
      connectionCheckInterval: 1000
    }, options);
    this.store = store;
    this.web3 = null;

    this.isConnected = false;
    this.currentAddress = null;

    setInterval(() => this.checkConnection(), this.options.connectionCheckInterval);
  }

  checkConnection() {
    // MetaMask inject a web3 instance into the page
    if (!window.web3) {
      return this.store.dispatch({ type: WEB3_METAMASK_MISSING });
    }
    // Initialize a web3 instance using the injected MetaMask one
    if (!this.web3) {
      this.web3 = new Web3(web3.currentProvider);
    }

    if (!this.isConnected && this.web3.isConnected()) {
      this.isConnected = true;
      this.refreshAccount()
        .then(({ address, network }) => this.store.dispatch({
          type: WEB3_CONNECTED,
          data: { address, network }
        }))
        .catch(error => this.store.dispatch({
          type: WEB3_CONNECTION_ERROR,
          error
        }));
    } else if (this.isConnected && !this.web3.isConnected()) {
      this.store.dispatch({ type: WEB3_DISCONNECTED });
      this.isConnected = false;
    }

    const address = this.getAccount();
    if (this.currentAddress !== address) {
      this.currentAddress = address;
      this.store.dispatch({
        type: WEB3_ADDRESS_CHANGED,
        data: { address }
      });
    }
  }

  refreshAccount() {
    return Promise.props({
      address: this.getAccount(),
      network: this.getNetwork()
    });
  }

  getNetwork() {
    return Promise.promisify(this.web3.version.getNetwork, {
      context: this.web3.version
    })();
  }

  getAccount() {
    return this.web3.eth.accounts[0];
  }

  getBalance(walletAddress) {
    return Promise.promisify(this.web3.eth.getBalance, {
      context: this.web3.eth
    })(walletAddress);
  }
}



export default Web3Wrapper;
