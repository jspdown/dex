'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class OrderNumber extends React.Component {
  static propTypes = {
    color: PropTypes.bool,
    type: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const [integer, floating, rest] = this.parseNumber(this.props.value);
    const valueClasses = classNames({
      'order-number': true,
      [this.props.type]: true,
      color: this.props.color
    });

    return (
      <div className={valueClasses}>
        <span className='integer'>{integer}</span>
        .
        <span className='floating'>{floating}</span>
        <span className='rest'>{rest}</span>
      </div>
    );
  }

  parseNumber(nbr, floatLength=8) {
    let [integer, floating] = nbr.split('.');
    let rest = '';
    let i = 0;

    if (floating.length > floatLength) {
      floating = (floating || '').slice(0, floatLength);
    }

    for (i = floating.length - 1; i >= 0; i--) {
      if (floating[i] !== '0') {
        floating = floating.slice(0, i);
        break;
      } else {
        rest += '0';
      }
    }
    if (i === -1) floating = '';

    for (i = 0; i < floatLength - (floating.length + rest.length); i++) {
      rest += '0';
    }

    return [integer, floating, rest];
  }
}

export default OrderNumber;
