'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { ZeroEx } from '0x.js';
import WalletIcon from './walletIcon.jsx';
import Spinner from './spinner.jsx';

class Account extends React.Component {
  static propTypes = {
    account: PropTypes.shape({
      isConnected: PropTypes.bool.isRequired,
      address: PropTypes.string,
      network: PropTypes.string
    }).isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const account = this.props.account;

    if (!account.isConnected) return <Spinner />;

    const shortAddress = account.address.slice(0, 6) + '...';

    return (
      <div className="wallet">
        <WalletIcon address={account.address} network={account.network} diameter={40} />
        <span className="address">{shortAddress}</span>
      </div>
    );
  }
}

export default Account;
