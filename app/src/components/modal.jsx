'use stric';

import React from 'react';
import classNames from 'classnames';
import MdClose from 'react-icons/lib/md/close';
import PropTypes from 'prop-types';
import Button from './button.jsx';

class Modal extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    onClose: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };

    this.close = this.close.bind(this);
    this.setContainer = this.setContainer.bind(this);
  }

  componentWillMount(callback) {
    setTimeout(() => this.setState({ isOpen: true }), 200);
  }

  close() {
    this.setState({ isOpen: false });
    setTimeout(() => this.props.onClose(), 200);
  }

  setContainer(ref) {
    this.container = ref;
  }

  render() {
    const overlayClasses = classNames({
      overlay: true,
      open: this.state.isOpen
    });
    const children = React.Children.map(this.props.children, child => {
      if (child.type === ModalHeader) {
        return React.cloneElement(child, { onClose: this.close });
      } else if (child.type === ModalFooter) {
        return React.cloneElement(child, { onCancel: this.close });
      }
      return child;
    });

    return (
      <div className={overlayClasses}>
        <div className='modal' ref={this.setContainer}>{children}</div>
      </div>
    );
  }
}

const ModalHeader = ({ children, onClose }) => (
  <div className='modal-header'>
    {children}
    {onClose && (<MdClose className='close' onClick={onClose} />)}
  </div>
);

const ModalFooter = ({ children, onCancel }) => {
  children = React.Children.map(children, child => {
    if (child.type === ModalCancelButton) {
      return React.cloneElement(child, { onCancel });
    }
    return child;
  })
  return (<div className='modal-footer'>{children}</div>);
}

const ModalBody = ({ children }) => (
  <div className='modal-body'>{children}</div>
);

const ModalCancelButton = ({ children, onCancel }) => (
  <Button role='secondary' onClick={onCancel}>{children}</Button>
);

export {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ModalCancelButton
};
