'use strict';

import React from 'react';
import PropTypes from 'prop-types';

class MoneyInput extends React.Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    currency: PropTypes.string.isRequired,
    amount: PropTypes.number
  };

  constructor(props) {
    super(props);

    this.state = {
      amount: this.props.amount
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    const amount = parseFloat(e.target.value);

    this.setState({ amount });
    this.props.onChange(amount);
  }

  render() {
    return (
      <div className='money-input'>
        <input
            name={this.props.currency}
            id={this.props.currency}
            type='number'
            value={this.state.amount || ''}
            placeholder='0.00'
            onChange={this.onChange} />
          <label htmlFor={this.props.currency} className='currency'>
            {this.props.currency}
          </label>
      </div>
    );
  }
}

export default MoneyInput;
