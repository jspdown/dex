'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Button extends React.Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    disabled: PropTypes.bool,
    role: PropTypes.oneOf(['primary', 'secondary'])
  };

  constuctor(props) {
    this.super(props);
  }

  render() {
    const role = this.props.role || 'primary';
    const buttonClasses = classNames({
      button: true,
      disabled: this.props.disabled,
      [role]: true
    });

    return (
      <button
        disabled={this.props.disabled}
        className={buttonClasses}
        onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

export default Button;
