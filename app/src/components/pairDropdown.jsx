'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class PairDropdown extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func.isRequired,
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    currencies: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  constructor(props) {
    super(props);

    this.menuElement = null;
    this.state = {
      from: props.from,
      to: props.to,
      isOpen: false,
      fromSelected: false,
      toSelected: false
    };

    this.toggle = this.toggle.bind(this);
    this.setContainer = this.setContainer.bind(this);
    this.clickHandler = this.clickHandler.bind(this);
    this.selectFrom = this.select.bind(this, 'from');
    this.selectTo = this.select.bind(this, 'to');
  }

  componentDidMount() {
    document.addEventListener('click', this.clickHandler, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.clickHandler, true);
  }

  clickHandler(e) {
    if (!this.state.isOpen) return;

    if (!this.menuElement.contains(e.target)) {
      this.setState({ isOpen: false });
      if (this.state.fromSelected || this.state.toSelected) {
        this.props.onSelect(this.state.from, this.state.to);
      }
    }
  }

  select(side, currency) {
    const twoSelection = (this.state.fromSelected && side === 'to')
                      || (this.state.toSelected && side === 'from');

    if (twoSelection) {
      this.props.onSelect(
        side === 'from' ? currency : this.state.from,
        side === 'to' ? currency : this.state.to
      );
      setTimeout(() => {
        this.setState({
          isOpen: false,
          fromSelected: false,
          toSelected: false
        });
      }, 200);
    }

    this.setState({
      [side]: currency,
      [`${side}Selected`]: true
    });
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  setContainer(ref) {
    this.menuElement = ref;
  }

  render() {
    const menuClasses = classNames({
      menu: true,
      visible: this.state.isOpen
    });

    return (
      <div className='pair-dropdown' ref={this.setContainer}>
        <div className='selection' onClick={this.toggle}>
          <span>{this.state.from}</span>
          /
          <span>{this.state.to}</span>
        </div>
        <div className={menuClasses}>
          <div>
            {this.props.currencies.map((currency, i) => (
              <div
                key={i}
                className={currency === this.state.from && 'selected'}
                onClick={() => this.selectFrom(currency)}>
                {currency}
              </div>
            ))}
          </div>
          <div>
            {this.props.currencies.map((currency, i) => (
              <div
                key={i}
                className={currency === this.state.to && 'selected'}
                onClick={() => this.selectTo(currency)}>
                {currency}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default PairDropdown;
