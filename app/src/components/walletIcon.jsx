'use strict';

import React from 'react';
import { findDOMNode } from 'react-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import jazzicon from 'jazzicon';
import { Networks } from '../constants/web3';

class WalletIcon extends React.Component {
  static propTypes = {
    address: PropTypes.string.isRequired,
    diameter: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const container = findDOMNode(this);
    this.updateIcon(container);
  }

  componentDidUpdate() {
    const container = findDOMNode(this);

    container.removeChild(container.children[0]);

    this.updateIcon(container);
  }

  updateIcon(container) {
    const address = this.props.address.slice(2, 10);
    const seed = parseInt(address, 16);

    const walletIcon = jazzicon(this.props.diameter, seed);

    container.append(walletIcon);
  }

  render() {
    const network = this.props.network;
    const classes = classNames({
      'wallet-icon': true,
      'white': network === Networks.MAIN_NET,
      'purple': network === Networks.KOVAN_TEST_NET,
      'red': network === Networks.ROSTEN_TEST_NEXT,
      'yellow': network === Networks.RINKEBY_TEST_NEXT
    })

    return (<div className={classes}></div>);
  }
}

export default WalletIcon;
