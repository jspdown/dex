'use strict';

import React from 'react';
import PropTypes from 'prop-types';

class SwitchButton extends React.Component {
  static propTypes = {
    values: PropTypes.arrayOf(PropTypes.string.isRequired),
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: this.props.value
    };

    this.onClick = this.onClick.bind(this);
  }

  onClick(value) {
    this.setState(prevState => {
      if (prevState.selected !== value) {
        this.props.onChange(value);
      }

      return { selected: value };
    });
  }

  render() {
    return (
      <div className='switch-button'>
        {this.props.values.map((value, i) => (
          <button
            key={i}
            className={this.state.selected === value && 'selected'}
            onClick={() => this.onClick(value)}>
            {value}
          </button>
        ))}
      </div>
    );
  }
}

export default SwitchButton;
