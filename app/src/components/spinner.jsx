'use strict';

import React from 'react';

class Spinner extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (<div className='spinner'></div>);
  }
}

export default Spinner;
