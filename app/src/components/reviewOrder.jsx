'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'
import MdRateReview from 'react-icons/lib/md/rate-review';
import Button from './button.jsx';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ModalCancelButton
} from './modal.jsx';

class ReviewOrder extends React.Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.onClose = this.onClose.bind(this);
    this.onCancel = this.onClose.bind(this);
    this.onExchange = this.onExchange.bind(this);
  }

  onClose() {
    this.props.history.push('/');
  }

  onExchange() { console.log('exchange'); }

  render() {
    return (
      <Modal onClose={this.onClose}>
        <ModalHeader>
          <MdRateReview className='icon success'/>
          Review details of your exchange
        </ModalHeader>
        <ModalBody>

        </ModalBody>
        <ModalFooter>
          <ModalCancelButton>Cancel</ModalCancelButton>
          <Button onClick={this.onExchange}>Exchange</Button>
        </ModalFooter>
      </Modal>
    );
  }
}


export default withRouter(ReviewOrder);
