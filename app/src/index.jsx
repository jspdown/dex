'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './containers/app.jsx';
import Web3Wrapper from './libs/web3';
import store from './store';

const web3Wraper = new Web3Wrapper(store);

render((
  <Provider store={store}>
    <BrowserRouter>
      <Route path='/' component={App} />
    </BrowserRouter>
  </Provider>
), document.getElementById('app'));
