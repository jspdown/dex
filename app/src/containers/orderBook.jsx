'use strict';

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '../components/button.jsx';
import OrderNumber from '../components/orderNumber.jsx';
import store from '../store';

class OrderBook extends React.Component {
  static propTypes = {
    type: PropTypes.oneOf(['ask', 'bid']).isRequired,
    pair: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      orders: [
        { price: '0.00081410',	size: '6560.37694777'	},
        { price: '0.00081412',	size: '154316.00000000' },
        { price: '0.00081415',	size: '12661.72227220' },
        { price: '0.00081425',	size: '8690.79527021'	},
        { price: '0.00081500',	size: '3600.00000000'	},
        { price: '0.00081888',	size: '10.48438926'	},
        { price: '0.00082029',	size: '925.39700000' },
        { price: '0.00082661',	size: '12.063033900000000' },
        { price: '0.00083811',	size: '923.59500000' },
        { price: '0.00083858',	size: '9.19931813' },
        { price: '0.00083936',	size: '188.00329952' },
        { price: '0.00083997',	size: '1.56554113' },
        { price: '0.00084365',	size: '11.02833840'	},
        { price: '0.00084614',	size: '0.67295472' },
        { price: '0.00084999',	size: '4523.59500000' }
      ]
    };
  }

  render() {
    const orderBookClasses = classNames('order-book', this.props.type);
    const type = this.props.type;

    return (
      <div className={orderBookClasses}>
        <div className='row header'>
          <span>size</span>
          <span>price</span>
        </div>
        {this.state.orders.map((order, i) => (
          <div className='row' key={i}>
            <OrderNumber type={type} value={order.size} />
            <OrderNumber color={true} type={type} value={order.price} />
          </div>
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(OrderBook);
