'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import OrderBook from './orderBook.jsx';
import CreateOrder from './createOrder.jsx';
import Account from '../components/account.jsx';
import ReviewOrder from '../components/reviewOrder.jsx';
import PairDropdown from '../components/pairDropdown.jsx';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pair: ['ZRX', 'WETH'],
      currencies: ['ZRX', 'DGD', 'WETH', 'MKR', 'REP', 'GNT', 'MLN']
    };

    this.onSelectPair = this.updatePair.bind(this);
  }

  updatePair(from, to) {
    this.setState({ pair: [from, to] });
  }

  render() {
    return (
      <div className='container'>
        <Route path='/review' component={ReviewOrder} />
        <header>
          <div className="header-center">
            <PairDropdown
              currencies={this.state.currencies}
              from={this.state.pair[0]}
              to={this.state.pair[1]}
              onSelect={this.onSelectPair} />
          </div>
          <div className="account">
            <Account account={this.props.account}/>
          </div>
          <h1>Stric</h1>
          <p>Decentralized by nature</p>
        </header>
        <div className='main'>
          <CreateOrder pair={this.state.pair} />
          <h2>Order book</h2>
          <div className='order-book-container'>
            <OrderBook type='bid' pair={this.state.pair} />
            <OrderBook type='ask' pair={this.state.pair} />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  account: state.account
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(App);
