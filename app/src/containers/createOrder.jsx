'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types';
import FaExchange from 'react-icons/lib/fa/exchange'
import Button from '../components/button.jsx';
import MoneyInput from '../components/moneyInput.jsx';
import SwitchButton from '../components/switchButton.jsx';

class CreateOrder extends React.Component {
  static propTypes = {
    pair: PropTypes.arrayOf(PropTypes.string).isRequired,
    history: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      amount: null,
      price: null,
      orderType: 'sell'
    };
    this.orderTypes = ['buy', 'sell'];

    this.postOrder = this.postOrder.bind(this);
    this.onAmountChanged = this.onAmountChanged.bind(this);
    this.onPriceChanged = this.onPriceChanged.bind(this);
    this.onSelectOrderType = this.onSelectOrderType.bind(this);
  }

  onAmountChanged(amount)       { this.setState({ amount }); }
  onPriceChanged(price)         { this.setState({ price }); }
  onSelectOrderType(orderType)  { this.setState({ orderType }); }

  postOrder() {
    this.props.history.push('/review', {
      orderType: this.state.orderType,
      pair: this.props.pair,
      amount: this.state.amount,
      price: this.state.price
    });
  }

  render() {
    const postOrderDisabled = !(this.state.amount > 0 && this.state.price > 0);

    return (
      <div className='create-order'>
        <div className='compact-view'>
          <SwitchButton
            values={this.orderTypes}
            value={this.state.orderType}
            onChange={this.onSelectOrderType} />
          <label>Amount</label>
          <MoneyInput
            amount={this.state.amount}
            currency={this.props.pair[0]}
            onChange={this.onAmountChanged}/>
          <label>Price</label>
          <MoneyInput
            amount={this.state.price}
            currency={this.props.pair[1]}
            onChange={this.onPriceChanged}/>
          <Button
            disabled={postOrderDisabled}
            onClick={this.postOrder}>
            {this.state.orderType}
          </Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateOrder));
