# DEX

# Start services: 

api: 

```
cd api;
npm start
```

app:

```
cd app
npm start
python server.py
```

# Intialize new environment:

create the database:

```
psql postgres
> CREATE DATABASE <name>;
```

run migration:

```
cd shared
npm run migrate
```

# Run migration:

```
cd shared
npm run migrate
```
