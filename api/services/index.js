'use strict';

const ProviderEngine = require('web3-provider-engine');
const RpcSubprovider = require('web3-provider-engine/subproviders/rpc');
const ZeroExService = require('./zeroEx');

/**
 * Bootstrap service:
 * - Create the web3 provider engine and initialize it
 * - Inject ZeroExService into OrderService
 *
 * @param  {String} ethereumRPCEndpoint Ethereum RPC node to use
 */
function bootstrap(ethereumRPCEndpoint) {
  const provider = new ProviderEngine();
  
  provider.addProvider(new RpcSubprovider({ rpcUrl: ethereumRPCEndpoint }));
  provider.start();

  module.exports.zeroExService = new ZeroExService(provider);
}

module.exports = { bootstrap };
