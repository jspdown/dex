'use strict';

const Promise = require('../../shared/libs/promise');
const { ZeroEx, ZeroExError } = require('0x.js');
const { Err, codes } = require('../../shared/errors');

const {
  ORDER_FULLY_FILLED,
  ORDER_EXPIRED,
  INSUFFICIENT_MAKER_ALLOWANCE,
  CONTRACT_NOT_FOUND
} = codes.VALIDATION;

/**
 * ZeroExService is a wrapper of the 0x.js library. All actions
 * requiring an interaction with the 0x protocol is delagated to this class
 */
class ZeroExService {
  /**
   * Instanciate a new ZeroExService and initialize a communication with
   * a web3 provider
   *
   * @param  {Object} provider  A web3 Provider
   * @return {ZeroExService}    Instanciated service
   */
  constructor(provider) {
    this.zeroEx = new ZeroEx(provider);
  }

  /**
   * Hash the order
   *
   * @param  {Object} order Order to hash
   * @return {String}       Hashed order
   */
  getOrderHash(order) {
    return ZeroEx.getOrderHashHex(order);
  }

  /**
   * Check if a signed order is fillable
   *
   * NOTE: This method doesn't check if the signature is correct.
   *
   * Errors that can be thrown:
   *  - ORDER_FULLY_FILLED: Order is completely filled or cancelled
   *  - ORDER_EXPIRED: Order has expired
   *  - INSUFFICIENT_MAKER_ALLOWANCE: Maker's allowance is not enough
   *  - CONTRACT_NOT_FOUND: Contract doesn't exists
   * ]
   * @param  {Object} signedOrder A signed order
   * @return {Promise}            Resolved once checked
   */
  checkSignedOrderIsFillable(signedOrder) {
    const errors = {
      [ZeroExError.OrderRemainingFillAmountZero]: ORDER_FULLY_FILLED,
      [ZeroExError.OrderFillExpired]: ORDER_EXPIRED,
      [ZeroExError.InsufficientMakerAllowance]: INSUFFICIENT_MAKER_ALLOWANCE,
      [ZeroExError.ContractDoesNotExist]: CONTRACT_NOT_FOUND
    };

    const validated = this.zeroEx.exchange
      .validateOrderFillableOrThrowAsync(signedOrder)
      .catch(err => {
        const code = errors[err.message] || codes.HTTP.INTERNAL_ERROR;

        throw new Err(code, { signedOrder });
      });

    return Promise.resolve(validated);
  }

  /**
   * Check if the given signature is valid according to the order hash and
   * the signer address.
   *
   * @param  {String}  orderHash     Hashed order
   * @param  {Object}  signature     ECDSA Signature of the order
   * @param  {String}  signerAddress Address that signed the order
   * @return {Boolean}               True if valid
   */
  isValidSignature(orderHash, signature, signerAddress) {
    return ZeroEx.isValidSignature(orderHash, signature, signerAddress);
  }
}

module.exports = ZeroExService;
