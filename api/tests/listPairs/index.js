'use strict';

const _ = require('lodash');
const { expect } = require('chai');
const { request } = require('../../../shared/tests/http-request');
const generate = require('../../../shared/tests/generators');
const fixture = require('../../../shared/tests/fixture')();
const { codes } = require('../../../shared/errors');
const { validation } = require('../../controllers/listPairs');
const helper = require('../helper');

describe('Route: GET /pairs', () => {
  describe('Unit', () => {
    describe('Validation', () => {
      /**
       * Verify if an error is thrown when an option is appended to the url.
       * The standard relayer api state that "Unsupported option" should be returned
       *
       * https://github.com/0xProject/standard-relayer-api#error-response
       */
      it('should return UNSUPPORTED_OPTION when an option is passed', () => {
        const req = {
          query: {
            option: 'value'
          }
        };

        return helper.testValidation(validation, req)
          .then(() => {
            throw new Error('Should have failed with UNSUPPORTED_OPTION');
          })
          .catch(err => {
            expect(err.code).to.equal(codes.VALIDATION.UNSUPPORTED_OPTION.id);
          });
      });
    });
  });

  describe('Integration', () => {
    const fixturesData = [
      generate.token('ZRX'),
      generate.token('MKR'),
      generate.token('GLM'),
      generate.token('WETH'),
      elm => generate.pair('ZRX_MKR', elm.ZRX, elm.MKR),
      elm => generate.pair('ZRX_GLM', elm.ZRX, elm.GLM),
      elm => generate.pair('MKR_GLM', elm.MKR, elm.GLM),
      elm => generate.pair('WETH_ZRX', elm.WETH, elm.ZRX, { isEnabled: false })
    ];

    before(() => fixture.load(fixturesData));
    after(() => fixture.purge());

    /**
     * Make a call on `GET /v0/token_pairs` and check if it returns
     * all token pairs that are available for trading. Once fetched, the response
     * format is checked to see if it satisfy the standard-relayer-api.
     *
     * https://github.com/0xProject/standard-relayer-api#get-v0token_pairs
     */
    it('should list all enabled pairs', () => {
      const token = [
        'id',
        'address',
        'symbol',
        'decimals',
        'minAmount',
        'maxAmount',
        'precision'
      ];

      return request('GET', '/pairs')
        .send()
        .expect(200)
        .then(response => {
          const ZRX = _.pick(Object.assign({}, {
            precision: 5,
            id: fixture.elements.ZRX._id.toString()
          }, fixture.elements.ZRX.toObject()), token);

          const GLM = _.pick(Object.assign({}, {
            precision: 5,
            id: fixture.elements.GLM._id.toString()
          }, fixture.elements.GLM.toObject()), token);

          const MKR = _.pick(Object.assign({}, {
            precision: 5,
            id: fixture.elements.MKR._id.toString()
          }, fixture.elements.MKR.toObject()), token);

          expect(response.body).to.have.deep.members([
            { base: ZRX, quote: MKR, id: fixture.elements.ZRX_MKR._id.toString() },
            { base: ZRX, quote: GLM, id: fixture.elements.ZRX_GLM._id.toString() },
            { base: MKR, quote: GLM, id: fixture.elements.MKR_GLM._id.toString() }
          ]);
        });
    });
  });
});
