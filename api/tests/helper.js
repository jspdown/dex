'use strict';

const _ = require('lodash');
const { ZeroEx } = require('0x.js');
const ethUtil = require('ethereumjs-util');
const config = require('../../shared/config');
const { chance } = require('../../shared/tests/generators');
const { validate } = require('../middlewares/validation');
const { NULL_ADDRESS } = require('../../shared/constants');

/**
 * Generate a signed order. The order hash and the signature are
 * correct. The signature can be used as it was issued by
 * `zeroEx.signOrderHashAsync`.
 *
 * @param  {Pair}   pair         Pair in which appears makerToken and takerToken
 * @param  {Token}  makerToken   Maker token
 * @param  {Token}  takerToken   Taker token
 * @param  {Object} [options={}] Override default values
 * @return {Object}              The signed order
 */
const generateFakeSignedOrder = (pair, makerToken, takerToken, options = {}) => {
  const makerWallet = {
    private: '0x09caa98af803640dd1336028ceeadcd6494d49730bba08de0cabc297f74a9f85',
    public: '0x2d7814b41b518b042cb228736906c21bef3906f9'
  };

  if ((options.maker && !options.private) || (!options.maker && options.private)) {
    throw new Error('Both the public and private key has to be provided');
  }
  const expiration = Math.trunc((Date.now() + config.order.duration) / 1000);
  const order = Object.assign({}, {
    maker: makerWallet.public,
    taker: NULL_ADDRESS,
    // Fees
    feeRecipient: config.order.feeRecipient,
    makerFee: '0',
    takerFee: '0',
    // Token contracts
    pairId: pair._id.toString(),
    exchangeContractAddress: config.zeroEx.exchangeContractAddress,
    // Amounts
    makerTokenAmount: ZeroEx.toBaseUnitAmount(
      chance.bigNumber({ min: 1, max: 2000 }),
      makerToken.decimals
    ).toFixed(),
    takerTokenAmount: ZeroEx.toBaseUnitAmount(
      chance.bigNumber({ min: 1, max: 2000 }),
      takerToken.decimals
    ).toFixed(),

    // Other fields
    salt: ZeroEx.generatePseudoRandomSalt().toFixed(),
    expirationUnixTimestampSec: String(expiration)
  }, _.omit(options, ['private']));

  const zeroExOrder = Object.assign({}, order);

  delete zeroExOrder.pairId;
  zeroExOrder.makerTokenAddress = makerToken.address;
  zeroExOrder.takerTokenAddress = takerToken.address;

  const orderHash = ZeroEx.getOrderHashHex(zeroExOrder);
  const orderHashBuffer = ethUtil.toBuffer(orderHash);
  const message = ethUtil.hashPersonalMessage(orderHashBuffer);
  const privateKey = ethUtil.toBuffer(options.private || makerWallet.private);
  const signedMessage = ethUtil.ecsign(message, privateKey);
  const signature = {
    r: ethUtil.bufferToHex(signedMessage.r),
    s: ethUtil.bufferToHex(signedMessage.s),
    v: signedMessage.v
  };

  return { order, orderHash, signature };
};

/**
 * Test validations rules against a request object
 *
 * @param  {Object[]} validations Validation rules
 * @param  {Object}   req         Express request
 * @return {Promise}              Resolved or rejected following the validation
 *                                result
 */
function testValidation(validations, req) {
  return new Promise((resolve, reject) => {
    const validator = validate(validations);

    validator(req, {}, err => (err ? reject(err) : resolve()));
  });
}

/**
 * Convert an object returned by mongoose into its stringified version
 *
 * @param  {Object} mongoObj Object returned by mongoose
 * @param  {Object} options  Convert or omit some properties
 * @return {Object}          Converted object
 */
function mongoToObj(mongoObj, options) {
  const propsToOmit = ['__v'];

  if (!options.timestamps) {
    propsToOmit.push('createdAt', 'updatedAt');
  }

  return JSON.parse(JSON.stringify(_.omit(mongoObj.toObject(), propsToOmit)));
}

function rawObj(obj) {
  return JSON.parse(JSON.stringify(obj));
}

module.exports = {
  generateFakeSignedOrder,
  testValidation,
  mongoToObj,
  rawObj
};
