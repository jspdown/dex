'use strict';

const _ = require('lodash');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const Promise = require('../../../shared/libs/promise');
const generate = require('../../../shared/tests/generators');
const fixture = require('../../../shared/tests/fixture')();
const db = require('../../../shared/models');
const { codes } = require('../../../shared/errors');
const { checkOrderIsFillable } = require('../../controllers/createOrder');
const { zeroExService } = require('../../services');
const helper = require('../helper');

chai.use(sinonChai);
const { expect } = chai;

function instanciateOrder(order, orderHash, ecSignature) {
  const expirationDate = new Date(
    parseInt(order.expirationUnixTimestampSec, 10) * 1000
  );

  return new db.Order(Object.assign({
    expirationDate,
    remainingAmount: order.takerTokenAmount,
    orderHash, ecSignature
  }, _.pick(order, [
    'maker', 'taker', 'pairId',
    'makerFee', 'takerFee',
    'makerTokenAmount', 'takerTokenAmount',
    'exchangeContractAddress', 'salt', 'feeRecipient'
  ])));
}

describe('Function: checkOrderIsFillable', () => {
  const fixturesData = [
    generate.token('ZRX'),
    generate.token('MKR'),
    elm => generate.pair('ZRX_MKR', elm.ZRX, elm.MKR)
  ];

  before(() => fixture.load(fixturesData));
  after(() => fixture.purge());

  /**
   * Check if the orderHash is checked against a new one build from the
   * body and some static fields (fees, address). If the orderHash is invalid,
   * INVALID_ORDER_HASH must be thrown.
   */
  it('should compare orderHash and throw INVALID_ORDER_HASH if invalid', done => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const orderInstance = instanciateOrder(order, orderHash, signature);

    sinon.stub(zeroExService, 'checkSignedOrderIsFillable')
      .callsFake(() => Promise.resolve());
    sinon.stub(zeroExService, 'getOrderHash')
      .callsFake(() => generate.chance.orderHash());

    checkOrderIsFillable(orderInstance)
      .then(() => done(new Error('should have failed')))
      .catchErr(codes.VALIDATION.INVALID_ORDER_HASH, () => {
        expect(zeroExService.getOrderHash).to.have.been.calledOnce;
        expect(zeroExService.checkSignedOrderIsFillable).to.not.have.been.called;

        done();
      })
      .catch(() => done(new Error('should throw INVALID_ORDER_HASH')))
      .finally(() => {
        zeroExService.checkSignedOrderIsFillable.restore();
        zeroExService.getOrderHash.restore();
      });
  });

  /**
   * Check if the expiration date of the order is higher than allowed. If it's
   * the case, the promise must be rejected with INVALID_EXPIRATION_DATE
   */
  it('should throw INVALID_EXPIRATION_DATE if the expiration date is higher than allowed', done => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const orderInstance = instanciateOrder(order, orderHash, signature);
    orderInstance.expirationDate = new Date(+orderInstance.expirationDate + 1000);

    sinon.stub(zeroExService, 'checkSignedOrderIsFillable')
      .callsFake(() => Promise.resolve());

    checkOrderIsFillable(orderInstance)
      .then(() => done(new Error('should have failed')))
      .catchErr(codes.VALIDATION.INVALID_EXPIRATION_DATE, () => done())
      .catch(() => done(new Error('should throw INVALID_EXPIRATION_DATE')))
      .finally(() => zeroExService.checkSignedOrderIsFillable.restore());
  });

  /**
   * Check if the expiration date is in the past. If it's the case the
   * Promise must be rejected with ORDER_EXPIRED
   */
  it('should throw ORDER_EXPIRED if the expiration date is in the past', done => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const orderInstance = instanciateOrder(order, orderHash, signature);
    orderInstance.expirationDate = new Date(Date.now() - 1000);

    sinon.stub(zeroExService, 'checkSignedOrderIsFillable')
      .callsFake(() => Promise.resolve());

    checkOrderIsFillable(orderInstance)
      .then(() => done(new Error('should have failed')))
      .catchErr(codes.VALIDATION.ORDER_EXPIRED, () => done())
      .catch(() => done(new Error('should throw INVALID_EXPIRATION_DATE')))
      .finally(() => zeroExService.checkSignedOrderIsFillable.restore());
  });

  /**
   * Check if the order is still fillable:
   * - verify if `orderService.checkSignedOrder` has been called correctly
   * - verify if the output of the function is correct
   */
  it('should check if the order is still fillable', () => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const orderInstance = instanciateOrder(order, orderHash, signature);

    sinon.stub(zeroExService, 'checkSignedOrderIsFillable')
      .callsFake(signedOrder => orderInstance.toZeroExSignedOrder()
        .then(expectedSignedOrder => expect(helper.rawObj(signedOrder))
          .to.eql(helper.rawObj(expectedSignedOrder))
        ));

    return checkOrderIsFillable(orderInstance)
      .then(() => {
        expect(zeroExService.checkSignedOrderIsFillable).to.have.been.calledOnce;
      })
      .finally(() => zeroExService.checkSignedOrderIsFillable.restore());
  });
});
