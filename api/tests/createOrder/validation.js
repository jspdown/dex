'use strict';

const _ = require('lodash');
const sinon = require('sinon');
const generate = require('../../../shared/tests/generators');
const fixture = require('../../../shared/tests/fixture')();
const { codes } = require('../../../shared/errors');
const { zeroExService } = require('../../services');
const helper = require('../helper');
const { expect } = require('chai');
const { validation } = require('../../controllers/createOrder');
const { convertOrderToInput } = require('./index');

describe('Validation', () => {
  const fixturesData = [
    generate.token('ZRX'),
    generate.token('MKR'),
    elm => generate.pair('ZRX_MKR', elm.ZRX, elm.MKR)
  ];

  before(() => {
    sinon.stub(zeroExService, 'checkSignedOrderIsFillable')
      .callsFake(() => Promise.resolve());

    return fixture.load(fixturesData);
  });
  after(() => {
    zeroExService.checkSignedOrderIsFillable.restore();

    return fixture.purge();
  });

  /**
   * Check if the UNSUPPORTED_OPTION is thrown when an option is passed
   * to the route.
   */
  it('should return UNSUPPORTED_OPTION if an query parameter is given', () => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const req = {
      body: convertOrderToInput(order, orderHash, signature),
      query: {
        value: 2
      }
    };

    return helper.testValidation(validation, req)
      .then(() => {
        throw new Error('Should have failed with UNSUPPORTED_OPTION');
      })
      .catch(error => {
        expect(error.code).to.equal(codes.VALIDATION.UNSUPPORTED_OPTION.id);
      });
  });

  /**
   * Check if REQUIRED_FIELD is thrown when a required field is missing
   */
  it('should return REQUIRED_FIELD if the field is required', () => {
    const requiredFields = [
      'maker',
      'pairId',
      'makerTokenAmount',
      'takerTokenAmount',
      'expirationDate',
      'salt',
      'orderHash',
      'ecSignature',
      'ecSignature.v',
      'ecSignature.r',
      'ecSignature.s'
    ];

    const fieldsChecked = requiredFields.map(field => {
      const { order, orderHash, signature } = helper.generateFakeSignedOrder(
        fixture.elements.ZRX_MKR,
        fixture.elements.ZRX,
        fixture.elements.MKR
      );

      const req = {
        body: convertOrderToInput(order, orderHash, signature),
        query: {}
      };

      // Remove a required field
      _.set(req.body, field, undefined);

      return helper.testValidation(validation, req)
        .then(() => {
          throw new Error('Should have failed with REQUIRED_FIELD');
        })
        .catch(error => {
          expect(error.code).to.equal(codes.VALIDATION.REQUIRED_FIELD.id);
        });
    });

    return Promise.all(fieldsChecked);
  });

  /**
   * Check if `maker is required to be an Ethereum address
   */
  it('should return INVALID_ADDRESS if `maker` is not an ethereum address', () => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const req = {
      body: convertOrderToInput(order, orderHash, signature),
      query: {}
    };
    // Degrate the body with an invalid maker value
    req.body.maker = '12345';

    return helper.testValidation(validation, req)
      .then(() => {
        throw new Error('Should have failed with INVALID_ADDRESS');
      })
      .catch(error => {
        expect(error.code).to.equal(codes.VALIDATION.INVALID_ADDRESS.id);
      });
  });

  /**
   * Check if `pairId` is required to be an ObjectIds
   */
  it('should return INVALID_ID if `pairId` is not an ObjectId', () => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const req = {
      body: convertOrderToInput(order, orderHash, signature),
      query: {}
    };
    // Degrate the body with an invalid pairId value
    req.body.pairId = '12345';

    return helper.testValidation(validation, req)
      .then(() => {
        throw new Error('Should have failed with INVALID_ID');
      })
      .catch(error => {
        expect(error.code).to.equal(codes.VALIDATION.INVALID_ID.id);
      });
  });

  /**
   * Check the type of `makerTokenAmount`, `takerTokenAmount` and `salt` and
   * ensure they are strings representing a number (BigNumber)
   */
  [
    'makerTokenAmount',
    'takerTokenAmount',
    'salt'
  ].forEach(field => {
    it(`should check that \`${field}\` must be a BigNumber`, () => {
      const { order, orderHash, signature } = helper.generateFakeSignedOrder(
        fixture.elements.ZRX_MKR,
        fixture.elements.ZRX,
        fixture.elements.MKR
      );
      const req = {
        body: convertOrderToInput(order, orderHash, signature),
        query: {}
      };
      // Degrate the body with an invalid property value
      req.body[field] = 'hello';

      return helper.testValidation(validation, req)
        .then(() => {
          throw new Error('Should have failed with INCORRECT_FORMAT');
        })
        .catch(error => {
          expect(error.code).to.equal(codes.VALIDATION.INCORRECT_FORMAT.id);
        });
    });
  });

  /**
   * Ensure the expirationDate is a date in a string format
   */
  it('should check that \'expirationDate\' is a date', () => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );

    const req = {
      body: convertOrderToInput(order, orderHash, signature),
      query: {}
    };

    req.body.expirationDate = new Date();

    return helper.testValidation(validation, req)
      .then(() => {
        throw new Error('Should have failed with INVALID_EXPIRATION_DATE');
      })
      .catch(error => {
        expect(error.code).to.equal(codes.VALIDATION.INVALID_EXPIRATION_DATE.id);
      });
  });

  /**
   * Ensure the orderHash must be an hexadecimal string. This test is not here
   * to verify if the orderHash is valid.
   */
  it('should check that \'orderHash\' is an hexadecimal string', () => {
    const { order, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );

    const orderHash = '0xabcdefi289w';
    const req = {
      body: convertOrderToInput(order, orderHash, signature),
      query: {}
    };

    return helper.testValidation(validation, req)
      .then(() => {
        throw new Error('Should have failed with INVALID_ORDER_HASH');
      })
      .catch(error => {
        expect(error.code).to.equal(codes.VALIDATION.INVALID_ORDER_HASH.id);
      });
  });

  it('should check the format of `ecSignature` and throw in case it\'s invalid INVALID_ECDSA', () => {
    const { order, orderHash, signature } = helper.generateFakeSignedOrder(
      fixture.elements.ZRX_MKR,
      fixture.elements.ZRX,
      fixture.elements.MKR
    );
    const invalidSignatureField = [
      { v: signature.s, r: signature.r, s: signature.s },
      { v: signature.v, r: signature.v, s: signature.s },
      { v: signature.v, r: signature.r, s: signature.v }
    ];

    const tested = invalidSignatureField.map(invalidSignature => {
      const req = {
        body: convertOrderToInput(order, orderHash, invalidSignature),
        query: {}
      };

      return helper.testValidation(validation, req)
        .then(() => {
          throw new Error('Should have failed with INVALID_ECDSA');
        })
        .catch(error => {
          expect(error.code).to.equal(codes.VALIDATION.INVALID_ECDSA.id);
        });
    });

    const reqInvalidSig = {
      body: convertOrderToInput(order, orderHash, 'string'),
      query: {}
    };

    tested.push(helper.testValidation(validation, reqInvalidSig)
      .then(() => {
        throw new Error('Should have failed with INVALID_ECDSA');
      })
      .catch(error => {
        expect(error.code).to.equal(codes.VALIDATION.INVALID_ECDSA.id);
      }));

    return Promise.all(tested);
  });
});
