'use strict';

const _ = require('lodash');
const sinon = require('sinon');
const { expect } = require('chai');
const { zeroExService } = require('../../services');
const { request } = require('../../../shared/tests/http-request');
const { NULL_ADDRESS } = require('../../../shared/constants');
const config = require('../../../shared/config');
const generate = require('../../../shared/tests/generators');
const fixture = require('../../../shared/tests/fixture')();
const db = require('../../../shared/models');
const helper = require('../helper');

function convertOrderToInput(order, orderHash, signature) {
  const expiration = new Date(
    parseInt(order.expirationUnixTimestampSec, 10) * 1000
  );

  return Object.assign({}, _.pick(order, [
    'maker', 'pairId',
    'makerTokenAmount', 'takerTokenAmount',
    'salt'
  ]), {
    orderHash,
    ecSignature: signature,
    expirationDate: expiration.toISOString()
  });
}

module.exports = { convertOrderToInput };

describe('Route: POST /orders', () => {
  describe('Unit', () => {
    require('./validation');
    require('./checkPairIsTradable');
    require('./checkOrderIsFillable');
  });

  describe('Integration', () => {
    const fixturesData = [
      generate.token('ZRX'),
      generate.token('MKR'),
      elm => generate.pair('ZRX_MKR', elm.ZRX, elm.MKR)
    ];

    before(() => {
      sinon.stub(zeroExService, 'checkSignedOrderIsFillable')
        .callsFake(() => Promise.resolve());

      return fixture.load(fixturesData);
    });

    after(() => {
      zeroExService.checkSignedOrderIsFillable.restore();

      return fixture.purge();
    });

    it('submit a new order', () => {
      const { order, orderHash, signature } = helper.generateFakeSignedOrder(
        fixture.elements.ZRX_MKR,
        fixture.elements.ZRX,
        fixture.elements.MKR
      );
      const body = convertOrderToInput(order, orderHash, signature);

      return request('POST', '/orders')
        .send(body)
        .expect(201)
        .then(res => {
          const orderId = res.body.id;
          const expectedOrder = {
            id: orderId,
            maker: body.maker,
            taker: NULL_ADDRESS,
            makerFee: '0',
            takerFee: '0',
            makerTokenAmount: body.makerTokenAmount,
            takerTokenAmount: body.takerTokenAmount,
            pairId: body.pairId,
            salt: body.salt,
            feeRecipient: config.order.feeRecipient,
            exchangeContractAddress: config.zeroEx.exchangeContractAddress,
            expirationDate: body.expirationDate,
            ecSignature: signature,
            orderHash: body.orderHash,
            state: 'OPEN',
            remainingAmount: body.takerTokenAmount
          };

          expect(res.body).to.eql(expectedOrder);

          return db.Order.findOne({ _id: orderId })
            .then(createdOrder => {
              expect(createdOrder.createdAt).to.exist;
              expect(createdOrder.updatedAt).to.exist;

              const rawCreatedOrder = helper.mongoToObj(createdOrder, {
                timestamps: false
              });

              expectedOrder._id = expectedOrder.id;
              delete expectedOrder.id;

              expect(rawCreatedOrder).to.eql(expectedOrder);
            });
        });
    });
  });
});
