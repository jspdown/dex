'use strict';

const { Schema } = require('mongoose');
const { expect } = require('chai');
const generate = require('../../../shared/tests/generators');
const fixture = require('../../../shared/tests/fixture')();
const { Err, codes } = require('../../../shared/errors');
const { checkPairIsTradable } = require('../../controllers/createOrder');

const { ObjectId } = Schema;

describe('Function: checkPairIsTradable', () => {
  const fixturesData = [
    generate.token('ZRX'),
    generate.token('MKR'),
    generate.token('REP'),
    elm => generate.pair('ZRX_MKR', elm.ZRX, elm.MKR),
    elm => generate.pair('REP_MKR', elm.REP, elm.MKR, { isEnabled: false })
  ];

  before(() => fixture.load(fixturesData));
  after(() => fixture.purge());

  /**
   * If a pair doesn't exist it must throw a TOKEN_NOT_FOUND
   */
  it('should fail if the pair is not tradable, because unknown', done => {
    checkPairIsTradable(ObjectId('59ecd67c10e0550f4aae430b'))
      .then(() => done(new Error('should not be tradable')))
      .catch(err => {
        expect(err).to.be.an.instanceOf(Err);
        expect(err.code).to.equal(codes.VALIDATION.PAIR_NOT_FOUND.id);
        done();
      })
      .catch(done);
  });

  /**
   * If a pair exists and is enabled for trading everything should goes well
   */
  it('should return true if the pair is tradable', () => {
    return checkPairIsTradable(fixture.elements.ZRX_MKR._id);
  });
});
