'use strict';

const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const Ajv = require('ajv');
const BigNumber = require('bignumber.js');
const { Err, ErrorCode, codes } = require('../../../shared/errors');
const {
  extendAjv,
  getError,
  convertValues,
  validate
} = require('../../middlewares/validation');

chai.use(sinonChai);
const { expect } = chai;

describe('Middleware: Validation', () => {
  const errors = {
    ADDRESS:        new ErrorCode(400, 1, 'address'),
    AMOUNT:         new ErrorCode(400, 2, 'amount'),
    AMOUNT_FORMAT:  new ErrorCode(400, 3, 'amount format'),
    DATE:           new ErrorCode(400, 4, 'date'),
    SUBOBJECT:      new ErrorCode(400, 5, 'subobject'),
    SUBOBJECT_NAME: new ErrorCode(400, 6, 'subobject')
  };
  const validationRule = {
    field: 'body',
    schema: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        address: { type: 'string', format: 'address' },
        amount: { type: 'string', format: 'big-number' },
        date: { type: 'string', format: 'date-time' },
        subObject: {
          type: 'object',
          properties: {
            name: { type: 'string' },
            number: { type: 'string', format: 'big-number' }
          },
          required: ['name', 'number'],
          additionalProperties: false
        }
      },
      required: ['name', 'address', 'amount', 'date', 'subObject'],
      additionalProperties: false
    },
    errors: {
      address: errors.ADDRESS,
      amount: {
        default: errors.AMOUNT,
        format: errors.AMOUNT_FORMAT
      },
      date: errors.DATE,
      subObject: {
        default: errors.SUBOBJECT,
        name: errors.SUBOBJECT_NAME
      }
    }
  };
  const fakeRequest = () => ({
    body: {
      name: 'name',
      address: '0x115d0a61820eef334aecdb3030858b7d326de556',
      amount: '12345678',
      date: '2017-10-19T10:32:56.000Z',
      subObject: {
        name: 'hello',
        number: '987654321'
      }
    }
  });

  describe('Function: getError', () => {
    const ajv = new Ajv();

    extendAjv(ajv);

    const compiledSchema = ajv.compile(validationRule.schema);
    const checkError = (req, expectedError) => {
      const isValid = compiledSchema(req.body);
      expect(isValid).to.equal(false);

      const error = getError(req, validationRule, compiledSchema);

      expect(error).to.an.instanceOf(Err);
      expect(error.code).to.equal(expectedError.id);
    };

    /**
     * When an error occurs, even if the error is not defined in the
     * `errors` object, an error should be thrown. This error must be
     * `INCORRECT_FORMAT`
     */
    it('should return INCORRECT_FORMAT if no errors match', () => {
      const req = fakeRequest();
      req.body.name = 1234;

      checkError(req, codes.VALIDATION.INCORRECT_FORMAT);
    });

    /**
     * AJV allows to defines which fields are required within an object.
     * Each time a required field is not define, REQUIRED_FIELD must be returned
     */
    it('should return REQUIRED_FIELD if a required field is missing', () => {
      const req = fakeRequest();
      delete req.body.name;

      checkError(req, codes.VALIDATION.REQUIRED_FIELD);
    });

    /**
     * When an error occurs an the error is defined the proper error should be
     * returned. For example if the error comes from `name`, and the errors object
     * is:
     * {
     *   errors: { name: ERROR_CODE }
     * }
     * `ERROR_CODE` should be returned
     */
    it('should return the error is defined: top level', () => {
      const req = fakeRequest();
      req.body.address = 'not and address';

      checkError(req, errors.ADDRESS);
    });

    /**
     * The `errors` object allows to define a specific error for attribute
     * of a field. This test cause an error on the format.
     */
    it('should return the error if defined: incorrect format', () => {
      const req = fakeRequest();
      req.body.amount = 'not an amount';

      checkError(req, errors.AMOUNT_FORMAT);
    });

    /**
     * The `errors` object allows to define a default error if we define
     * errors that are specific to an attribute. This test ensure the proper
     * error is returned.
     */
    it('should return the error if defined: default error', () => {
      const req = fakeRequest();
      req.body.amount = 1234;

      checkError(req, errors.AMOUNT);
    });

    /**
     * The `getError` function must handle errors that may occur on a deep object
     * This test cause an error on an attribute of the sub object and on the sub object
     * itself.
     */
    it('should return the error if defined: sub object', () => {
      const req = fakeRequest();
      req.body.subObject.name = 1234;

      checkError(req, errors.SUBOBJECT_NAME);

      req.body.subObject = 2;

      checkError(req, errors.SUBOBJECT);
    });
  });

  describe('Function: convertValues', () => {
    /**
     * Check if all the values are converted.
     * A big number written is a string format must be converted to a BigNumber
     * object. Same for string representing dates. This test also ensure it works
     * on deep object
     */
    it('should convert values correctly', () => {
      const req = fakeRequest();
      const converted = convertValues(validationRule.schema, req.body);

      expect(converted).to.have.all.keys(Object.keys(req.body));
      expect(converted.name).to.equal(req.body.name);
      expect(converted.address).to.equal(req.body.address);
      expect(converted.amount).to.be.an.instanceOf(BigNumber);
      expect(converted.amount.toString()).to.equal(req.body.amount);
      expect(converted.date).to.be.an.instanceOf(Date);
      expect(converted.date.toISOString()).to.equal(req.body.date);
      expect(converted.subObject.name).to.equal(req.body.subObject.name);
      expect(converted.subObject.number).to.be.an.instanceOf(BigNumber);
      expect(converted.subObject.number.toString()).to.equal(req.body.subObject.number);
    });
  });

  describe('Function: validate', () => {
    const validator = validate([validationRule]);

    /**
     * Check if an error is correctly thrown if the validation failed. It also
     * check if the `next` callback is only called once
     */
    it('should call next with the error if not valid', () => {
      const req = fakeRequest();
      req.body.address = 1234;
      const next = sinon.spy();

      validator(req, {}, error => {
        expect(error).to.be.an.instanceOf(Err);
        expect(error.code).to.equal(errors.ADDRESS.id);
        next();
      });

      expect(next).to.have.been.calledOnce;
    });

    /**
     * Check if request fields is correctly converted if the validation succeed.
     * it Also check if the `next` callback is only called once
     */
    it('should call next with no value if valid and should have converted field', () => {
      const req = fakeRequest();
      const next = sinon.spy();

      validator(req, {}, error => {
        expect(error).to.be.undefined;
        next();
      });

      expect(next).to.have.been.calledOnce;
      expect(req.body.amount).to.be.an.instanceOf(BigNumber);
    });
  });
});
