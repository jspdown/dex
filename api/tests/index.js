'use strict';

process.env.NODE_ENV = 'test';

const logger = require('../../shared/logger');
const db = require('../../shared/models');
const httpRequest = require('../../shared/tests/http-request');
const config = require('../../shared/config');

require('../services').bootstrap(config.ethereumRPCEndpoint);

const api = require('../api');

function onError(err) {
  logger.error(err, { stack: err.stack });
  db.close();
  process.exit(1);
}

describe('API:', () => {
  db.connect(config.database)
    .then(() => api.listen(config.port))
    .then(() => httpRequest.init(api))
    .then(() => {
      require('./middlewares');
      require('./listPairs');
      require('./createOrder');

      return run(onError);
    })
    .catch(onError);
});
