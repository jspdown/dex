'use strict';

const express = require('express');
const { codes, Err } = require('../shared/errors');
const use = require('./middlewares/use');
const controllers = require('./controllers');

const routeNotFound = (req, res, next) =>
  next(new Err(codes.HTTP.NOT_FOUND));

const router = express.Router();

router
  .get('/pairs', use(controllers.listPairs))
  .post('/orders', use(controllers.createOrder))
  .use('*', routeNotFound);

module.exports = router;
