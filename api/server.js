'use strict';

const logger = require('../shared/logger').init('API');
const config = require('../shared/config');
const db = require('../shared/models');
const api = require('./api');

require('./services').bootstrap(config.ethereumRPCEndpoint);

db.connect(config.database)
  .then(() => {
    api.listen(config.port);
    logger.info('Server started', { port: config.port });
  })
  .catch(err => logger.error('Something went wrong!', err));
