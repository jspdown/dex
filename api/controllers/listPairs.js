'use strict';

const _ = require('lodash');
const db = require('../../shared/models');
const { codes } = require('../../shared/errors');

const validation = [{
  field: 'query',
  schema: {
    type: 'object',
    additionalProperties: false
  },
  errors: {
    default: codes.VALIDATION.UNSUPPORTED_OPTION
  }
}];

/**
 * List available pairs of token. A pair is allowed to be
 * traded only if `isEnabled` is true
 *
 * @param  {Object}   req  Express request
 * @param  {Object}   res  Express response
 * @param  {Function} next Completion callback
 * @return {Promise}       Resolved once fetched
 */
function controller(req, res, next) {
  return db.Pair.find({ isEnabled: true })
    .populate(['quoteId', 'baseId'])
    .lean()
    .exec()
    .then(tokenPairs => {
      req.status = 200;
      req.data = tokenPairs;
    })
    .asCallback(next);
}

/**
 * Templates the token pairs we just fetched.
 * The output format is fully compatible with the standard-relayer-api:
 *
 * https://github.com/0xProject/standard-relayer-api#get-v0token_pairs
 *
 * @param  {TokenPair[]} pairs Array of token pairs
 * @return {Object}            Formated token pairs
 */
function template(pairs) {
  const tokenFields = [
    'address',
    'symbol',
    'decimals',
    'minAmount',
    'maxAmount',
    'precision'
  ];
  const templateToken = token => Object.assign(
    {},
    { precision: 5, id: token._id.toString() },
    _.pick(token, tokenFields)
  );

  return pairs.map(pair => ({
    id: pair._id.toString(),
    base: templateToken(pair.baseId),
    quote: templateToken(pair.quoteId)
  }));
}

module.exports = { validation, controller, template };
