'use strict';

const _ = require('lodash');
const Promise = require('../../shared/libs/promise');
const db = require('../../shared/models');
const { Err, codes } = require('../../shared/errors');
const config = require('../../shared/config');
const { NULL_ADDRESS } = require('../../shared/constants');
const { zeroExService } = require('../services');

const validation = [{
  field: 'query',
  schema: {
    type: 'object',
    additionalProperties: false
  },
  errors: {
    default: codes.VALIDATION.UNSUPPORTED_OPTION
  }
}, {
  field: 'body',
  schema: {
    type: 'object',
    properties: {
      maker: { type: 'string', format: 'address' },
      pairId: { type: 'string', format: 'object-id' },
      makerTokenAmount: { type: 'string', format: 'big-number' },
      takerTokenAmount: { type: 'string', format: 'big-number' },
      salt: { type: 'string', format: 'big-number' },
      expirationDate: { type: 'string', format: 'date-time' },
      orderHash: { type: 'string', format: 'hexadecimal' },
      ecSignature: {
        type: 'object',
        properties: {
          v: { type: 'number' },
          r: { type: 'string' },
          s: { type: 'string' }
        },
        required: ['v', 'r', 's'],
        additionalProperties: false
      }
    },
    required: [
      'maker', 'pairId',
      'makerTokenAmount', 'takerTokenAmount', 'salt',
      'expirationDate', 'orderHash', 'ecSignature'
    ],
    additionalProperties: false
  },
  errors: {
    maker: codes.VALIDATION.INVALID_ADDRESS,
    pairId: codes.VALIDATION.INVALID_ID,
    ecSignature: codes.VALIDATION.INVALID_ECDSA,
    orderHash: codes.VALIDATION.INVALID_ORDER_HASH,
    expirationDate: codes.VALIDATION.INVALID_EXPIRATION_DATE
  }
}];

/**
 * Create an order:
 * - Check if the pair is tradable
 * - Check if the order is fillable
 * - Insert the order in the database
 *
 * `remainingAmount` is intialized with the `takerTokenAmount`. This assumption
 * can be wrong since an order may has been filled partially in the past.
 * This amount will be updated by the pruning process.
 *
 * Errors:
 * - VALIDATION.INVALID_EXPIRATION_DATE
 * - VALIDATION.ORDER_EXPIRED
 * - VALIDATION.INVALID_ORDER_HASH
 * - VALIDATION.INVALID_ECDSA
 * - VALIDATION.ORDER_FULLY_FILLED
 * - VALIDATION.INSUFFICIENT_MAKER_ALLOWANCE
 * - VALIDATION.CONTRACT_NOT_FOUND
 * - VALIDATION.PAIR_NOT_FOUND
 *
 * @param  {Object}   req  Express request
 * @param  {Object}   res  Express response
 * @param  {Function} next Completion callback
 * @return {Promise}       Resolved once fetched
 */
function controller(req, res, next) {
  const {
    maker, pairId,
    makerTokenAmount, takerTokenAmount,
    salt, expirationDate,
    orderHash, ecSignature
  } = req.body;

  const order = new db.Order({
    pairId,
    maker,
    taker: NULL_ADDRESS,
    makerFee: '0',
    takerFee: '0',
    feeRecipient: config.order.feeRecipient,
    makerTokenAmount, takerTokenAmount,
    exchangeContractAddress: config.zeroEx.exchangeContractAddress,
    remainingAmount: takerTokenAmount,
    salt, expirationDate,
    orderHash, ecSignature
  });

  return checkPairIsTradable(pairId)
    .then(() => checkOrderIsFillable(order))
    .then(() => order.save())
    .then(() => {
      req.status = 201;
      req.data = order.toObject();
    })
    .asCallback(next);
}

/**
 * Check order hash and if the order is still fillable:
 * - signature is correct
 * - order is not yet fully filled
 * - order is not expired
 * - maker allowance is enough to cover the order
 * - token contract exists
 *
 * Throws:
 * - VALIDATION.INVALID_EXPIRATION_DATE: Expiration date is not valid
 * - VALIDATION.ORDER_EXPIRED: Order is already expired
 * - VALIDATION.INVALID_ORDER_HASH: Order hash is invalid
 * - VALIDATION.INVALID_ECDSA: Signature is invalid
 * - VALIDATION.ORDER_FULLY_FILLED: Order is already filled
 * - VALIDATION.INSUFFICIENT_MAKER_ALLOWANCE: Maker allowance is not enough
 * - VALIDATION.CONTRACT_NOT_FOUND: Taker or maker contact doesn't exist
 *
 * @param  {Object} req Express request
 */
function checkOrderIsFillable(order) {
  const now = Date.now();
  const maxExpirationDate = now + config.order.duration;
  const { orderHash, ecSignature, maker, expirationDate } = order.toObject();

  // Ensure the expiration date is not higher than allowed
  if (+expirationDate > maxExpirationDate) {
    return Promise.reject(new Err(codes.VALIDATION.INVALID_EXPIRATION_DATE, {
      order: order.toObject()
    }));
  }
  // Ensure the expiration date is not from the past

  if (+expirationDate <= now) {
    return Promise.reject(new Err(codes.VALIDATION.ORDER_EXPIRED, {
      order: order.toObject()
    }));
  }
  return order.toZeroExOrder()
    .then(zeroExOrder => {
      // Hash the order and check if they are the same
      const expectedOrderHash = zeroExService.getOrderHash(zeroExOrder);
      if (orderHash !== expectedOrderHash) {
        throw new Err(codes.VALIDATION.INVALID_ORDER_HASH, {
          order: order.toObject()
        });
      }

      const signedOrder = Object.assign({ ecSignature }, zeroExOrder);
      // Check if the signature is valid compared to the order hash
      if (!zeroExService.isValidSignature(orderHash, ecSignature, maker)) {
        throw new Err(codes.VALIDATION.INVALID_ECDSA, { signedOrder });
      }
      // Check if the order is still fillable
      return zeroExService.checkSignedOrderIsFillable(signedOrder);
    });
}

/**
 * Check if the given pair is tradable. A pair is tradable if
 * it an entry exist in the database and if this pair is enabled
 *
 * Throws:
 * - VALIDATION.PAIR_NOT_FOUND: Pair is not allowed for trade
 *
 * @param  {ObjectId} pairId  Pair id (base/quote)
 */
function checkPairIsTradable(pairId) {
  return db.Pair.findOne({ _id: pairId })
    .then(tokenPair => {
      if (!tokenPair || !tokenPair.isEnabled) {
        throw new Err(codes.VALIDATION.PAIR_NOT_FOUND, { pairId });
      }
    });
}

/**
 * Templates the order we've just created.
 *
 * @param  {Order} order  Created order
 * @return {Object}       Formated order
 */
const template = order => Object.assign(
  { id: order._id },
  _.pick(order, [
    'maker',
    'taker',
    'makerFee',
    'takerFee',
    'makerTokenAmount',
    'takerTokenAmount',
    'pairId',
    'exchangeContractAddress',
    'salt',
    'feeRecipient',
    'expirationDate',
    'ecSignature',
    'orderHash',
    'state',
    'remainingAmount'
  ])
);

module.exports = {
  checkOrderIsFillable,
  checkPairIsTradable,
  validation,
  controller,
  template
};
