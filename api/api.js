'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const errorHandler = require('./middlewares/error');
const router = require('./router');

const app = express();

if (process.env === 'production') {
  app.use(morgan('tiny'));
}

app
  .disable('x-powered-by')
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(cors())
  .use(router)
  .use(errorHandler);

module.exports = app;
