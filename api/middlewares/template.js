'use strict';

/**
 * Generate an express middleware to template a API response. The
 * template function takes as input the response of the controller (stored
 * in `req.status` and `req.data`) and returns the templated version.
 *
 * @param  {Function} renderer Templates an API response
 * @return {Function}          Generated middleware
 */
function template(renderer) {
  return (req, res, next) => {
    const status = req.status || 200;
    const data = renderer(req.data || {});

    return res
      .status(status)
      .send(data);
  };
}

module.exports = template;
