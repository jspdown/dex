'use strict';

const logger = require('../../shared/logger');
const { codes, Err } = require('../../shared/errors');

/**
 * Express middleware to handle errors. Errors inheriting from `Err`
 * are considered to be handled and known so their status will be used in the
 * response.
 * If the error is unknown, a INTERNAL_ERROR will be returned.
 *
 * All errors of the api follow this schema:
 * ```
 * {
 *   name: A string describing the type of error,
 *   message: A string that describe more deeply what occured
 *   stack: The stack trace
 * }
 * ```
 *
 * TODO:
 * - Disable the stack trace in production env
 * - Add a data field to provide the context of the error
 *
 * @param  {Error}    err  Error caugth
 * @param  {Object}   req  Request
 * @param  {Object}   res  Response
 * @param  {Function} next Done callback (not used)
 */
function errorHandler(err, req, res, next) {
  let error = err;

  if (error instanceof SyntaxError && error.status === 400 && 'body' in error) {
    error = new Err(codes.COMMON.MALFORMED_JSON);
  } else if (!(error instanceof Err)) {
    logger.error(codes.HTTP.INTERNAL_ERROR, {
      name: error.name,
      message: error.message,
      stack: error.stack
    });

    error = new Err(codes.HTTP.INTERNAL_ERROR);
  }

  res.status(error.status || 400).send({ error: error.toJSON() });
}

module.exports = errorHandler;
