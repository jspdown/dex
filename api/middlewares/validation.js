'use strict';

const _ = require('lodash');
const Ajv = require('ajv');
const BigNumber = require('bignumber.js');
const { PATTERNS } = require('../../shared/constants');
const { codes, Err, ErrorCode } = require('../../shared/errors');

/**
 * Determine which error to return. Each validation rule may define
 * a `errors` field to specify which code to throw following the situation.
 * It takes this form:
 *
 * {
 *   errors: {
 *     default: <code>,       // When nothing better matches
 *     fieldName: {
 *       default: <code>,     // in case nothing matches locally
 *       keywordName: <code>  // if both field and keyword caused the error
 *     },
 *     anotherField: <code>   // no matter the keyword
 *   }
 * }
 *
 * @param  {Object} req            Express request
 * @param  {Object} validationRule Validation rules that caused the error
 * @param  {Object} compiledSchema Compiled version of the schema
 * @return {Error}                 eError that best fit the situation
 */
function getError(req, validationRule, compiledSchema) {
  const error = compiledSchema.errors[0];
  const errorPayload = {
    data: req[validationRule.field],
    info: error.message
  };

  if (error.keyword === 'required') {
    return new Err(codes.VALIDATION.REQUIRED_FIELD, errorPayload);
  }

  // Remove first element: `dataPath` always start with a `.`
  const fieldParts = error.dataPath.split('.').slice(1);
  // Generate the reverse list of path where the error can be found:
  // [a, b, c] => [a.b.c, a.b, a]
  const paths = fieldParts.reduce((acc, part, i, parts) => {
    acc.push(parts.slice(0, parts.length - i).join('.'));

    return acc;
  }, []);

  for (let i = 0; i < paths.length; i++) {
    const fieldError = _.get(validationRule.errors, paths[i]);

    if (!fieldError) continue;

    if (fieldError instanceof ErrorCode) {
      return new Err(fieldError, errorPayload);
    } else if (_.isObject(fieldError)) {
      const keywordError = fieldError[error.keyword];
      // The field is specifying different error following the `keyword`.
      // A keyword is an attribute of the property (eg `maxLength`)
      // If no key word matches, the `default` field will be used
      if (keywordError instanceof ErrorCode) {
        return new Err(keywordError, errorPayload);
      } else if (fieldError.default instanceof ErrorCode) {
        return new Err(fieldError.default, errorPayload);
      }
    }
  }

  return new Err(
    validationRule.errors.default || codes.VALIDATION.INCORRECT_FORMAT,
    errorPayload
  );
}

/**
 * Extend AJV capabilities
 *
 * @param  {Ajv} ajv Ajv instance to extend
 */
function extendAjv(ajv) {
  // Validate Ethereum address
  ajv.addFormat('address', PATTERNS.ADDRESS);
  // Validate Number
  ajv.addFormat('big-number', PATTERNS.NUMBER);
  // Validate Hex strings
  ajv.addFormat('hexadecimal', PATTERNS.HEX);
  // Validate ObjectId
  ajv.addFormat('object-id', PATTERNS.OBJECTID);
}

/**
 * Convert values if needed:
 * - string format: big-number => BigNumber
 * - string format: date-time => Date
 *
 * @param  {Object}               schema Validation schema
 * @param  {Object|String|Number} value  Value to convert
 * @return {Object|String|Number}        Converted value
 */
function convertValues(schema, value) {
  const stringFormatConverter = {
    'big-number': v => new BigNumber(v),
    'date-time': v => new Date(v)
  };

  // Travers recursively the schema
  if (schema.type === 'object' && !_.isUndefined(schema.properties)) {
    const converted = {};

    Object.keys(schema.properties)
      .forEach(field => {
        if (!_.isUndefined(value[field])) {
          converted[field] = convertValues(schema.properties[field], value[field]);
        }
      });

    return converted;
  }
  // Convert strings if the format tells we need to
  if (schema.type === 'string') {
    const converter = stringFormatConverter[schema.format];

    return converter ? converter(value) : value;
  }

  return value;
}

/**
 * Generate an express middleware to validate an incoming request. This generator
 * takes an array of validation steps where each step can validate a portion
 * of the request. eg: body, query, params
 *
 * This middleware only understands AJV validation schemas
 * http://epoberezkin.github.io/ajv/
 *
 * @param  {Object[]} validations Validation steps
 * @return {Function}             The generated middleware
 */
function validate(validations) {
  const ajv = new Ajv();

  extendAjv(ajv);

  const compiledSchemas = validations.map(({ schema }) => ajv.compile(schema));

  return (req, res, next) => {
    for (let i = 0; i < validations.length; i++) {
      const isValid = compiledSchemas[i](req[validations[i].field]);

      if (!isValid) {
        const error = getError(req, validations[i], compiledSchemas[i]);

        return next(error);
      }

      req[validations[i].field] = convertValues(
        validations[i].schema,
        req[validations[i].field]
      );
    }

    return next();
  };
}

module.exports = {
  extendAjv,
  getError,
  convertValues,
  validate
};
