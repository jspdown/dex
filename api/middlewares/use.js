'use strict';

const { compose } = require('compose-middleware');
const { Err, HTTP } = require('../../shared/errors');
const { validate } = require('./validation');
const template = require('./template');

/**
 * Express middleware to build a route handler. A route can be composed of:
 * - a validation middleware to validate the input request (body, params)
 * - a controller which contains the logic of the route
 *
 * @param  {Object} resource Route definition
 * @return {Function}        The composed middleware
 */
function use(resource) {
  const middlewares = [];

  if (resource.validation) {
    middlewares.push(validate(resource.validation));
  }

  if (!resource.controller) {
    throw new Err(HTTP.NOT_IMPLEMENTED, { step: 'controller' });
  }
  middlewares.push(resource.controller);

  if (!resource.template) {
    throw new Err(HTTP.NOT_IMPLEMENTED, { step: 'template' });
  }
  middlewares.push(template(resource.template));

  return compose(middlewares);
}

module.exports = use;
