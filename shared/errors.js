'use strict';

class Err extends Error {
  constructor(code = HTTP.INTERNAL_ERROR, data = {}) {
    super(code.message);

    this.name = this.constructor.name;
    this.data = data;
    this.message = code.message;
    this.code = code.id;
    this.status = code.status;

    if (typeof(Error.captureStackTrace) === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(code.message)).stack;
    }
  }

  toJSON() {
    return {
      code: this.code,
      status: this.status,
      message: this.message,
      data: this.data
    };
  }
}

class ErrorCode {
  constructor(status, id, message) {
    this.status = status;
    this.id = id;
    this.message = message;
  }
}

const code = (id, message, status) => new ErrorCode(status || 400, id, message);

const HTTP = {
  BAD_REQUEST:      code(0, 'Bad request', 400),
  NOT_FOUND:        code(1, 'Not found', 404),
  INTERNAL_ERROR:   code(2, 'Internal error', 500),
  NOT_IMPLEMENTED:  code(3, 'Not Implemented', 501)
};

const COMMON = {
  MALFORMED_JSON:   code(10, 'Malformed JSON')
};

const VALIDATION = {
  REQUIRED_FIELD:               code(1000, 'Required field'),
  INCORRECT_FORMAT:             code(1001, 'Incorrect format'),
  INVALID_ADDRESS:              code(1002, 'Invalid address'),
  ADDRESS_NOT_SUPPORTED:        code(1003, 'Address not supported'),
  OUT_OF_RANGE:                 code(1004, 'Value out of range'),
  INVALID_ECDSA:                code(1005, 'Invalid ECDSA or Hash'),
  UNSUPPORTED_OPTION:           code(1006, 'Unsupported option'),
  INVALID_ORDER_HASH:           code(1007, 'Invalid order hash'),
  ORDER_FULLY_FILLED:           code(1008, 'Order is fully filled'),
  ORDER_EXPIRED:                code(1009, 'Order has expired'),
  INSUFFICIENT_MAKER_ALLOWANCE: code(1010, 'Insufficient maker allowance'),
  CONTRACT_NOT_FOUND:           code(1011, 'Contract not found'),
  INVALID_EXPIRATION_DATE:      code(1012, 'Invalid expiration date'),
  INVALID_ID:                   code(1013, 'Invalid id'),
  PAIR_NOT_FOUND:               code(1014, 'Pair not found')
};

module.exports = {
  Err, ErrorCode,
  codes: { HTTP, COMMON, VALIDATION }
};
