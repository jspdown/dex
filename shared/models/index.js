'use strict';

const Promise = require('../libs/promise');
const mongoose = require('mongoose');
const { Token } = require('./token');
const { Pair } = require('./pair');
const { Order } = require('./order');

mongoose.Promise = Promise;

/**
 * Connect to the mongodb database
 *
 * @param  {String} uri       The database uri
 * @param  {Object} options   Mongoose connection options
 * @return {Promise}          Resolved when the connection is open.
 */
function connect(uri, options = {}) {
  mongoose.connect(uri, Object.assign({
    useMongoClient: true
  }, options));

  return new Promise((resolve, reject) => {
    mongoose.connection.on('error', reject);
    mongoose.connection.on('open', resolve);
  });
}

/**
 * Close mongoose connection to the DB
 */
function close() {
  mongoose.connection.close();
}

module.exports = {
  connect,
  close,
  Token,
  Pair,
  Order
};
