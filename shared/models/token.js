'use strict';

const mongoose = require('mongoose');

const TokenSchema = new mongoose.Schema({
  name: { type: String },
  symbol: { type: String },
  address: { type: String },
  decimals: { type: Number },
  minAmount: { type: String },
  maxAmount: { type: String }
}, { timestamps: true });

const Token = mongoose.model('Token', TokenSchema);

module.exports = { Token, TokenSchema };
