'use strict';

const _ = require('lodash');
const mongoose = require('mongoose');
const { ORDER_STATE } = require('../constants');

const ObjectId = mongoose.Schema.ObjectId;
const OrderSchema = new mongoose.Schema({
  state: {
    type: String,
    enum: [
      ORDER_STATE.OPEN,
      ORDER_STATE.EXPIRED,
      ORDER_STATE.CLOSED,
      ORDER_STATE.UNFUNDED
    ],
    default: ORDER_STATE.OPEN
  },
  maker: { type: String },
  taker: { type: String },
  makerFee: { type: String },
  takerFee: { type: String },
  makerTokenAmount: { type: String },
  takerTokenAmount: { type: String },
  pairId: { type: ObjectId, ref: 'Pair' },
  remainingAmount: { type: String },
  salt: { type: String },
  exchangeContractAddress: { type: String },
  feeRecipient: { type: String },
  expirationDate: { type: Date },
  orderHash: { type: String },
  ecSignature: {
    r: { type: String },
    s: { type: String },
    v: { type: Number }
  }
}, { timestamps: true });

OrderSchema.methods.toZeroExOrder = toZeroExOrder;
OrderSchema.methods.toZeroExSignedOrder = toZeroExSignedOrder;

const Order = mongoose.model('Order', OrderSchema);

/**
 * Convert the order into a 0x order
 * https://0xproject.com/docs/0xjs#Order
 *
 * @return {Object} Ox order
 */
function toZeroExOrder() {
  // Clone the object to prevent the instance pairId to be replaced
  // by its populated version
  return new Order(this)
    .populate({
      path: 'pairId',
      select: ['baseId', 'quoteId'],
      populate: [
        { path: 'baseId', select: 'address' },
        { path: 'quoteId', select: 'address' }
      ]
    })
    .execPopulate()
    .then(order => Object.assign({
      expirationUnixTimestampSec: String(Math.trunc(+this.expirationDate / 1000)),
      makerTokenAddress: _.get(order, 'pairId.baseId.address'),
      takerTokenAddress: _.get(order, 'pairId.quoteId.address')
    }, _.pick(order, [
      'maker',
      'taker',
      'makerTokenAmount',
      'takerTokenAmount',
      'exchangeContractAddress',
      'makerFee',
      'takerFee',
      'feeRecipient',
      'salt'
    ])));
}

/**
 * Convert the order into a 0x signed order
 * https://0xproject.com/docs/0xjs#SignedOrder
 *
 * @return {Object} 0x signed order
 */
function toZeroExSignedOrder() {
  return this.toZeroExOrder()
    .then(zeroExOrder => Object.assign({
      ecSignature: this.ecSignature
    }, zeroExOrder));
}

module.exports = { Order, OrderSchema };
