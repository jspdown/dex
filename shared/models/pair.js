'use strict';

const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;

const PairSchema = new mongoose.Schema({
  baseId: { type: ObjectId, ref: 'Token' },
  quoteId: { type: ObjectId, ref: 'Token' },
  isEnabled: { type: Boolean, default: true }
}, { timestamps: true });

const Pair = mongoose.model('Pair', PairSchema);

module.exports = { Pair, PairSchema };
