'use strict';

const _ = require('lodash');
const env = process.env.NODE_ENV || 'dev';

function getEnv(name) {
  if (!process.env[name]) {
    throw new Error(`Missing env variable '${name}'`);
  }

  return process.env[name];
}

let config = {
  port: 4242,
  database: getEnv('MONGODB_URI'),
  order: {
    // Period of validity of an order: 2 days
    duration: 1000 * 60 * 60 * 24 * 2,
    feeRecipient: getEnv('FEE_RECIPIENT')
  },
  zeroEx: {
    // TODO: retrieve this address using `zeroEx.exchange.getContractAddressAsync`
    exchangeContractAddress: '0xb69e673309512a9d726f87304c6984054f87a93b'
  },
  ethereumRPCEndpoint: getEnv('ETHEREUM_RPC_ENDPOINT')
};

if (env === 'test') {
  _.set(config, 'database', 'mongodb://localhost/dex_test');
  _.set(config, 'ethereumRPCEndpoint', 'http://localhost:8545');
} else if (env === 'production') {
  _.set(config, 'database.options.logging', false);
}


module.exports = config;
