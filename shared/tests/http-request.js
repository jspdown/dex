'use strict';

const supertest = require('supertest');

let endpoint;

const init = api => endpoint = supertest(api);

function request(method, path) {
  const methodName = method.toLowerCase();

  return endpoint[methodName](path);
}

module.exports = { request, init };
