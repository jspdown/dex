'use strict';

process.env.NODE_ENV = 'test';

const logger = require('../logger');
const db = require('../models');
const config = require('../config');

function onError(err) {
  logger.error(err, { stack: err.stack });
}

describe('Shared:', () => {
  db.connect(config.database)
    .then(() => {
      require('./libs');

      return run(onError);
    })
    .catch(onError)
    .finally(() => db.close());
});
