'use strict';

const { expect } = require('chai');
const Promise = require('../../libs/promise');
const { Err, ErrorCode } = require('../../../shared/errors');

const CODE1 = new ErrorCode(400, 100, 'error code');
const CODE2 = new ErrorCode(400, 101, 'error code');

describe('Library: Promise', () => {
  it('should extends Bluebird', () => {
    expect(Promise.bind).to.not.be.undefined;
  });

  describe('Method: catchErr', () => {
    it('should provide a `catchErr` method', () => {
      expect(Promise.prototype.catchErr).to.not.be.undefined;
    });

    it('should not be invoked when no error is thrown', done => {
      Promise.resolve()
        .catchErr(CODE1, () => done(new Error('should have not invoked `catchErr`')))
        .then(() => done());
    });

    it('should not be invoked when the error is not instance of ErrorCode', done => {
      Promise.reject(new Error())
        .catchErr(CODE1, () => done(new Error('should have not invoked `catchErr`')))
        .then(() => done(new Error('should have failed')))
        .catch(() => done());
    });

    it('should not be invoked when the error is an ErrorCode but not matching', done => {
      Promise.reject(new Err(CODE1))
        .catchErr(CODE2, () => done(new Error('should have not invoked `catchErr`')))
        .then(() => done(new Error('should have failed')))
        .catch((err) => done());
    });

    it('should be invocked when the error is an ErrorCode and match', done => {
      Promise.reject(new Err(CODE1))
        .then(() => done('should have invoked `.catchErr`'))
        .catchErr(CODE1, () => done());
    });
  });
});
