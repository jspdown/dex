'use strict';

const chance = require('chance')();
const { Token } = require('../../models');

const generateToken = (name, options) => ({
  name,
  model: Token,
  data: Object.assign({
    name,
    symbol: name,
    address: chance.ethereumAddress(),
    decimals: chance.integer({ min: 1, max: 20 }),
    minAmount: '0',
    maxAmount: '100'
  }, options)
});

module.exports = generateToken;
