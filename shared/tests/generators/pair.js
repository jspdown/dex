'use strict';

const { Pair } = require('../../models');

const generatePair = (name, base, quote, options) => {
  return {
    name,
    model: Pair,
    data: Object.assign({
      isEnabled: true,
      baseId: base._id,
      quoteId: quote._id
    }, options),
  };
};

module.exports = generatePair;
