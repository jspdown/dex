'use strict';

const chance = require('chance')();
const BigNumber = require('bignumber.js');
const token = require('./token');
const pair = require('./pair');

const HEX_ALPHABET = '0123456789abcdef';

chance.mixin({
  ethereumAddress: () => '0x' + chance.string({
    length: 40,
    pool: HEX_ALPHABET
  }),
  bigNumber: options => new BigNumber(chance.integer(options)),
  orderHash: () => '0x' + chance.string({
    length: 64,
    pool: HEX_ALPHABET
  })
});

module.exports = {
  chance,
  token,
  pair
};
