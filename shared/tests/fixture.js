'use strict';

const _ = require('lodash');
const Promise = require('bluebird');

/**
 * Class that allow fixture loading and purging
 */
class Fixture {
  constructor() {
    this.models = [];
    this.elements = {};
  }

  /**
   * Load a list of fixtures:
   * type: [fixture]
   * where fixture can either be an object or an array of:
   * - object: { name: <name>, model: <Model>,  data: <content> }
   * - function: (fixtures) => (object)
   *
   * Function can help to create complexe creation like linking
   * a user to a campaign since the have access to previous inserted fixtures.
   *
   * @param  {Array} elements An array of fixture
   * @return {Promise}        Resolved once loaded
   */
  load(elements) {
    return elements.reduce((previous, element) => {
      return previous.then(() => {
        if (_.isFunction(element)) {
          return Promise.resolve(element(this.elements))
            .then(_element => this.loadElement(_element));
        }
        return this.loadElement(element);
      });
    }, Promise.resolve());
  }

  /**
   * Purge all collections used earlier.
   *
   * @param   {Array}   models  Array of models
   * @return  {Promise}         Resolved once purged
   */
  purge(models) {
    if (models) {
      models.forEach(model => this.registerModel(model));
    }

    return Promise.resolve(this.models)
      .each(model => model.collection.drop())
      .catch(err => this.onError(err, 'purging'))
      .finally(() => {
        this.models = [];
        this.elements = {};
      });
  }

  /**
   * Create a new document with the given element.
   * An element is an object of this type:
   * { name: <name>, model: <Model>, data: <data> }
   *
   * - `<name>` will be the key of the document in `this.elements`
   * - `<Model>` a mongoose model (eg: User)
   * - `<data>` An object used to initialize the document
   *
   * @param  {Object} element The element to create
   * @return {Promise}        A promise resolved after the creation
   */
  loadElement(element) {
    if (_.isArray(element)) {
      return this.loadElements(element);
    }

    this.registerModel(element.model);

    return element.model.create(element.data)
      .then(data => this.elements[element.name] = data);
  }

  /**
   * Load many elements
   *
   * @param  {Object} elements  The elements to create
   * @return {Promise}          A promise resolved after the creation
   */
  loadElements(elements) {
    return Promise.all(elements.map(e => this.loadElement(e)));
  }

  /**
   * Register a collection

   * @param  {Object} model An Mongoose model
   */
  registerModel(model) {
    if (!this.models.includes(model)) {
      this.models.push(model);
    }
  }

  /**
   * Helper method to handle error.

   * @param  {Error} err          An instance of error to log
   * @param  {string} action      The name of the action
   */
  onError(err, action) {
    const errorMessage = JSON.stringify(err, ['message', 'stack'], 2);

    logger.log(`Error while '${action}' fixtures: \n${errorMessage}`);
    throw err;
  }
}

/**
 * Helper that instanciate a new Fixture for you
 *
 * @return {Fixture}  The newly created fixture.
 */
function create() {
  return new Fixture();
}


module.exports = create;
