'use strict';

const PATTERNS = {
  ADDRESS: '^0x[0-9a-f]{40}$',
  NUMBER: '^[0-9]+$',
  HEX: '^0x[0-9-a-f]+$',
  OBJECTID: '^[a-f\\d]{24}$'
};

const ORDER_STATE = {
  OPEN: 'OPEN',
  EXPIRED: 'EXPIRED',
  CLOSED: 'CLOSED',
  UNFUNDED: 'UNFUNDED'
};

const NULL_ADDRESS = '0x0000000000000000000000000000000000000000';

module.exports = { PATTERNS, ORDER_STATE, NULL_ADDRESS };
