'use strict';

let name = 'UNKNOWN';

function init(serviceName) {
  name = serviceName;

  return module.exports;
}

function info(message, data) {
  console.log(`[${name}]: ${format(message)}`, JSON.stringify(data, null, 2));
}

function error(error, message) {
  console.error(`[${name}]: ${format(error)} ${formatError(message)}`);
}

function format(data) {
  if (typeof data === 'string') return data;

  if (data.id) {
    let message = `id=${data.id}`;

    if (data.status) message += ` status=${data.status}`;
    if (data.message) message += ` msg=${data.message}`;

    return message
  } else {
    return JSON.stringify(data, null, 2);
  }
}

function formatError(error) {
  if (!error) return '';

  return `
  name: "${error.name}"
  message: "${error.message}"
  stack: ${error.stack}
  `;
}

module.exports = { init, info, error };
