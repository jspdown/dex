'use strict';

const bluebird = require('bluebird/js/release/promise')();
const { Err } = require('../../shared/errors');

/**
 * Add a `catchErr` method to bluebird. This method allows us to catch
 * a specific error code (instance of ErrorCode).
 *
 * @param  {ErrorCode} errorCode The error code to catch
 * @param  {Function}  func      Function to execute when the code matches
 * @return {Object}              The chain of promises
 */
bluebird.prototype.catchErr = function catchErr(errorCode, func) {
  return this.then(undefined, err => {
    if (err instanceof Err && err.code === errorCode.id) {
      return func(err);
    }

    throw err;
  });
};

module.exports = bluebird;
