'use strict';

const config = {
  awaitPoolingInterval: 100, // Every seconds
  fee: {
    recipient: '0xd463511fd569eb4e390a36536a6e64036c22a7e5',
    maker: 0.005, // 0.5%
    taker: 0.005, // 0.5%
  },
  contracts: {
    exchange: '0x90fe2af704b34e0224bf2299c838e04d4dcf1364'
  },
  orderDuration: 60 * 60 * 24 * 2, // 2 days
  tokens: {
    ZRX: {
      name: '0x Protocol Token',
      symbol: 'ZRX',
      decimals: 18,
      address: '0x6ff6c0ff1d68b964901f986d4c9fa3ac68346570'
    },
    WETH: {
      name: 'Ether Token',
      symbol: 'WETH',
      decimals: 18,
      address: '0x05d090b51c40b020eab3bfcb6a2dff130df22e9c'
    }
  }
};

window.addEventListener('load', async () => {
  ZeroEx = ZeroEx.ZeroEx;

  const zeroEx = new ZeroEx(web3.currentProvider, {});

  const createOrderButton = document.getElementById('createOrder');
  const fillOrderButton = document.getElementById('fillOrder');
  const cancelOrderButton = document.getElementById('cancelOrder');
  const portalButton = document.getElementById('portal');
  const textarea = document.getElementById('order');
  const makerAmountInput = document.getElementById('makerAmount');
  const takerAmountInput = document.getElementById('takerAmount');

  let currentOrder = null;

  setInterval(() => {
    zeroEx.exchange.getLogsAsync('LogCancel', {}, {
      feeRecipient: config.fee.recipient
    })
      .then(a => console.log('cancel ===>', a))
      .catch(e => console.log(e))
  }, 3000);

  createOrderButton.addEventListener('click', async () => {
    const makerAmount = new BigNumber(makerAmountInput.value);
    const takerAmount = new BigNumber(takerAmountInput.value);
    const makerAddresses = await zeroEx.getAvailableAddressesAsync();

    const { order, orderHash, signature } = await createOrder(
      zeroEx,
      makerAddresses[0], null,
      config.tokens.ZRX, makerAmount,
      config.tokens.WETH, takerAmount
    );

    getRemainingAmountToBeFilled(zeroEx, orderHash)
      .then(a => console.log(a.toString()))
      .catch(err => console.error(err));

    const formated = convertToPortalFormat(order, orderHash, signature);

    currentOrder = formated;
    textarea.innerText = JSON.stringify(formated, null, 2);
  });

  portalButton.addEventListener('click', () => {
    const url = `https://0xproject.com/portal/fill?order=${JSON.stringify(currentOrder)}`;
    const win = window.open(url, '_blank');

    win.focus();
  });

  fillOrderButton.addEventListener('click', () => fillOrder(currentOrder));
  cancelOrderButton.addEventListener('click', () => cancelOrder(currentOrder));
});

const computeMakerFee = amount => new BigNumber('0');
const computeTakerFee = amount => new BigNumber('0');
const getExpirationDate = () => {
  const expiration = Math.round(Date.now() / 1000) + config.orderDuration;

  return new BigNumber(expiration);
};

async function createOrder(zeroEx, makerAddress, takerAddress, tokenA, makerAmount, tokenB, takerAmount) {
  const order = {
    maker: makerAddress,
    taker: takerAddress || ZeroEx.NULL_ADDRESS,
    // fees
    feeRecipient: config.fee.recipient,
    makerFee: computeMakerFee(makerAmount),
    takerFee: computeTakerFee(takerAmount),
    // token contacts
    makerTokenAddress: tokenB.address,
    takerTokenAddress: tokenA.address,
    exchangeContractAddress: config.contracts.exchange,
    // amounts
    makerTokenAmount: ZeroEx.toBaseUnitAmount(takerAmount, tokenB.decimals),
    takerTokenAmount: ZeroEx.toBaseUnitAmount(makerAmount, tokenA.decimals),
    // other
    salt: ZeroEx.generatePseudoRandomSalt(),
    expirationUnixTimestampSec: getExpirationDate()
  };

  const orderHash = ZeroEx.getOrderHashHex(order);
  const signature = await zeroEx.signOrderHashAsync(orderHash, makerAddress);

  return { order, orderHash, signature };
}

function getRemainingAmountToBeFilled(zeroEx, orderHash) {
  return zeroEx.exchange.getFilledTakerAmountAsync(orderHash);
}

function convertToPortalFormat(order, orderHash, signature) {
  const getTokenName = address => Object.keys(config.tokens)
    .find(tokenName => config.tokens[tokenName].address === address);
  const makerToken = config.tokens[getTokenName(order.makerTokenAddress)];
  const takerToken = config.tokens[getTokenName(order.takerTokenAddress)];

  return {
    maker: {
      address: order.maker,
      token: makerToken,
      amount: order.makerTokenAmount.toString(),
      feeAmount: order.makerFee.toString()
    },
    taker: {
      address: order.taker,
      token: takerToken,
      amount: order.takerTokenAmount.toString(),
      feeAmount: order.takerFee.toString()
    },
    expiration: order.expirationUnixTimestampSec.toString(),
    feeRecipient: order.feeRecipient,
    salt: order.salt.toString(),
    signature: {
      v: signature.v,
      r: signature.r,
      s: signature.s,
      hash: orderHash
    },
    exchangeContract: order.exchangeContractAddress,
    networkId: 42
  };
}



function fillOrder() {
  console.log('fillOrder');
}

function cancelOrder() {
  console.log('cancelOrder');
}
